package xyz.coincoin.dvd;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.RandomXS128;

public class DVDGame extends ApplicationAdapter {

    enum State {
        MOVING, PAUSE
    }

    public static final float SQUARE_SIZE = 50;
    public static final float SCREEN_HEIGHT = 720;
    public static final float SCREEN_WIDTH = 1280;

    ShapeRenderer shapeRenderer;
    Color color;
    RandomXS128 randomXS128;
    float x;
    float y;
    float deltaX;
    float deltaY;
    State state = State.MOVING;

    @Override
    public void create() {
        shapeRenderer = new ShapeRenderer();
        randomXS128 = new RandomXS128();
        randomColor();
        x = SCREEN_WIDTH / 2 - SQUARE_SIZE / 2;
        y = SCREEN_HEIGHT / 2 - SQUARE_SIZE / 2;
        randomDirection();
    }

    private void randomColor() {
        Color currentColor = color;

        do {
            color = new Color(randomXS128.nextFloat(), randomXS128.nextFloat(), randomXS128.nextFloat(), 1);
        } while (color.equals(currentColor));
    }

    private void randomDirection() {
        switch (randomXS128.nextInt(8)) {

            case 0:
                deltaX = 1;
                deltaY = 0;
                break;
            case 1:
                deltaX = 1;
                deltaY = -1;
                break;
            case 2:
                deltaX = 0;
                deltaY = -1;
                break;
            case 3:
                deltaX = -1;
                deltaY = -1;
                break;
            case 4:
                deltaX = -1;
                deltaY = 0;
                break;
            case 5:
                deltaX = -1;
                deltaY = 1;
                break;
            case 6:
                deltaX = 0;
                deltaY = 1;
                break;
            case 7:
                deltaX = 1;
                deltaY = 1;
                break;
        }
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(color);

        if (state == State.MOVING) {
            moveSquare();
        }

        if (Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
            if (state == State.MOVING) {
                state = State.PAUSE;
            } else {
                state = State.MOVING;
            }
        }

        shapeRenderer.rect(x, y, SQUARE_SIZE, SQUARE_SIZE);
        shapeRenderer.end();
    }

    private void moveSquare() {
        x = x + deltaX;
        y = y + deltaY;

        if (x < 0) {
            x = 0;
            deltaX = 1;
            randomColor();
        } else if (x > SCREEN_WIDTH - SQUARE_SIZE) {
            x = SCREEN_WIDTH - SQUARE_SIZE;
            deltaX = -1;
            randomColor();
        }
        if (y < 0) {
            y = 0;
            deltaY = 1;
            randomColor();
        } else if (y > SCREEN_HEIGHT - SQUARE_SIZE) {
            y = SCREEN_HEIGHT - SQUARE_SIZE;
            deltaY = -1;
            randomColor();
        }
    }

    @Override
    public void dispose() {
        shapeRenderer.dispose();
    }
}
