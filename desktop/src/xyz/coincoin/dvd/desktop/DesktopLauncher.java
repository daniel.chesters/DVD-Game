package xyz.coincoin.dvd.desktop;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import xyz.coincoin.dvd.DVDGame;

public class DesktopLauncher {
    public static final int SCREEN_HEIGHT = 720;
    public static final int SCREEN_WIDTH = 1280;

    public static void main(String[] arg) {
        Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
        config.setTitle("A DVD Game");
        config.setWindowedMode(SCREEN_WIDTH, SCREEN_HEIGHT);
        config.setResizable(false);
        config.setWindowIcon("DVD_icon.png");
        new Lwjgl3Application(new DVDGame(), config);
    }
}
